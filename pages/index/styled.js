import styled from 'styled-components';

// Wrapper
// ============================================================
const IndexStyle = styled.div`
  /* Parent styles
  ------------------------------- */
  width: auto;

  .home {
    h3 {
      padding-top: 10px;
    }
  }
`;

export default IndexStyle;
