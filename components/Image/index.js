import classNames from 'classnames';
import { Fragment } from 'react';
import { ImageStyle, ImageButtonStyle } from './styled';

export default function Image({ src, alt, isCircle, className, onClcik, onMouseOver, ...props }) {
  return (
    <Fragment>
      {onClcik ? (
        <ImageButtonStyle className={classNames('image-wrapper', { is_circle: isCircle }, className)} onClick={onClcik} onMouseOver={onMouseOver}>
          <img className="image" src={src} alt={alt} {...props} />
        </ImageButtonStyle>
      ) : (
        <ImageStyle className={classNames('image-wrapper', { is_circle: isCircle }, className)}>
          <img className="image" src={src} alt={alt} {...props} />
        </ImageStyle>
      )}
    </Fragment>
  );
}

// Image.defaultProps = {
//   width: 20,
//   height: 20,
// }
