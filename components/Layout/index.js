import classNames from 'classnames';
import _ from 'lodash';
import { useRef } from 'react';
import { Navbar } from 'components';
import { LayoutStyle } from './styled';

export default function Layout({ children, className }) {
  const divRef = useRef(null);

  return (
    <LayoutStyle className={classNames('layout', className)} bodyHeight={_.get(divRef, 'current.scrollHeight')}>
      <Navbar />

      <div ref={divRef} className={classNames('main')}>
        <div className="main-content-wrapper">{children}</div>
      </div>
    </LayoutStyle>
  );
}
